# Atom Elementary Dark UI

This theme is meant to make atom fit in with the rest of Elementary OS when using a dark theme.
In order to make it feel as native as possible, most of the styling has been derived directly from the official GTK theme!

Requires that you use a dark syntax theme as well, because it attempts to match the syntax themes background color.
